package crudIPiezas.Service;

import java.util.List;

import crudIPiezas.Dto.Proveedores;



public interface IProveedoresService {
	public List<Proveedores> listarProveedores();
	
	public Proveedores guardarProveedor(Proveedores proveedores);	
	
	public Proveedores proveedorXID(int id); 

	public Proveedores actualizarProveedor(Proveedores proveedores); 
	
	public void eliminarProveedor(int id);
}
