package crudIPiezas.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import crudIPiezas.Dao.SuministraDao;
import crudIPiezas.Dto.Suministra;
@Service
public class SuministraServiceImpl implements ISuministraService{
	@Autowired
	SuministraDao suministraDao;

	@Override
	public List<Suministra> listarSuministra() {
		// TODO Auto-generated method stub
		return suministraDao.findAll();
	}

	@Override
	public Suministra guardarSuministra(Suministra suministra) {
		// TODO Auto-generated method stub
		return suministraDao.save(suministra);
	}

	@Override
	public Suministra suministraXID(int id) {
	
		return suministraDao.findById(id).get();
	}

	@Override
	public Suministra actualizarSuministra(Suministra suministra) {

		return suministraDao.save(suministra);
	}

	@Override
	public void eliminarSuministra(int id) {
		suministraDao.deleteById(id);
		
	}


}
