package crudIPiezas.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import crudIPiezas.Dao.PiezasDao;
import crudIPiezas.Dto.Piezas;
@Service
public class PiezasServiceImpl implements IPiezasService{

	@Autowired
	PiezasDao piezasDao;

	@Override
	public List<Piezas> listarPiezas() {

		return piezasDao.findAll();
	}

	@Override
	public Piezas guardarPiezas(Piezas piezas) {
		// TODO Auto-generated method stub
		return piezasDao.save(piezas);
	}

	@Override
	public Piezas piezasXID(int id) {
		// TODO Auto-generated method stub
		return piezasDao.findById(id).get();
	}

	@Override
	public Piezas actualizarPiezas(Piezas piezas) {
		// TODO Auto-generated method stub
		return piezasDao.save(piezas);
	}

	@Override
	public void eliminarPiezas(int id) {
		piezasDao.deleteById(id);
		
	}
	
	

}
