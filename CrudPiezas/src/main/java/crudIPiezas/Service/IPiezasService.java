package crudIPiezas.Service;

import java.util.List;

import crudIPiezas.Dto.Piezas;

public interface IPiezasService {
	public List<Piezas> listarPiezas();
	
	public Piezas guardarPiezas(Piezas piezas);	
	
	public Piezas piezasXID(int id); 
	
	public Piezas actualizarPiezas(Piezas piezas); 
	
	public void eliminarPiezas(int id);

}
