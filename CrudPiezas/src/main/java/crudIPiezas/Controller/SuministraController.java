package crudIPiezas.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import crudIPiezas.Dto.Suministra;
import crudIPiezas.Service.SuministraServiceImpl;
@RestController
@RequestMapping("/api")
public class SuministraController {

	@Autowired
	SuministraServiceImpl SuministraServiceImpl;
	
	@GetMapping("/Suministra")
	public List<Suministra> listarSuministra(){
		return SuministraServiceImpl.listarSuministra();
	}
	
	@PostMapping("/Suministra")
	public Suministra guardarSuministra(@RequestBody Suministra Suministra) {
		return SuministraServiceImpl.guardarSuministra(Suministra);
	}
	
	@GetMapping("/Suministra/{id}")
	public Suministra SuministraXID(@PathVariable(name="id") int id) {
		
		Suministra Suministra_xid= new Suministra();
		
		Suministra_xid=SuministraServiceImpl.suministraXID(id);
		
		System.out.println("Suministra XID: "+Suministra_xid);
		
		return Suministra_xid;
	}
	
	
	@PutMapping("/Suministra/{id}")
	public Suministra actualizarSuministra(@PathVariable(name="id")int id,@RequestBody Suministra Suministra) {
		
		Suministra Suministra_seleccionado= new Suministra();
		Suministra Suministra_actualizado= new Suministra();
		
		Suministra_seleccionado= SuministraServiceImpl.suministraXID(id);
		
		Suministra_seleccionado.setPiezas(Suministra.getPiezas());
		Suministra_seleccionado.setProveedores(Suministra.getProveedores());
		
		Suministra_actualizado = SuministraServiceImpl.actualizarSuministra(Suministra_seleccionado);
		
		System.out.println("El Suministra actualizado es: "+ Suministra_actualizado);
		
		return Suministra_actualizado;
	}

	@DeleteMapping("/Suministra/{id}")
	public void eliminarSuministra(@PathVariable(name="id")int id) {
		SuministraServiceImpl.eliminarSuministra(id);
	}

}
