package crudIPiezas.Dao;

import org.springframework.data.jpa.repository.JpaRepository;

import crudIPiezas.Dto.Suministra;

public interface SuministraDao extends JpaRepository<Suministra, Integer>{

}
