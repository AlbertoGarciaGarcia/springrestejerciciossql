package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Productos;
import com.example.demo.service.ProductoServiceImpl;

@RestController
@RequestMapping("/api")
public class ProductosController {

	@Autowired
	ProductoServiceImpl productoserviceImpl;
	
	@GetMapping("/productos")
	public List<Productos> listarproductos() {
		return productoserviceImpl.listarProductos();
		
	}
	
	@PostMapping("/productos")
	public Productos guardarproducto(@RequestBody Productos producto) {
		return productoserviceImpl.guardarProducto(producto);
		
	}
	
	@GetMapping("/productos/{id}")
	public Productos productoID(@PathVariable(name = "id") int id) {
		
		Productos producto_id = new Productos();
		
		producto_id = productoserviceImpl.productoID(id);
		
		System.out.println("producto ID: " + producto_id);
		
		return producto_id;
		
	}
	
	@PutMapping("/productos/{id}")
	public Productos actualizarproducto(@PathVariable(name = "id") int id, @RequestBody Productos producto) {
		
		Productos producto_sel = new Productos();
		Productos producto_act = new Productos();
		
		producto_sel = productoserviceImpl.productoID(id);
		
		producto_sel.setNombre(producto.getNombre());
		producto_sel.setPrecio(producto.getPrecio());
		
		producto_act = productoserviceImpl.actualizarProducto(producto_sel);
		
		return producto_act;
		
	}
	
	@DeleteMapping("/productos/{id}")
	public void eliminarProducto(@PathVariable(name = "id") int id) {
		productoserviceImpl.eliminarProducto(id);
	}
}
