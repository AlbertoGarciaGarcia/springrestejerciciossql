package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.MaquinasRegistradas;
import com.example.demo.service.MaquinasRegistradasServiceImpl;

@RestController
@RequestMapping("/api")
public class MaquinasRegistradasController {

	@Autowired
	MaquinasRegistradasServiceImpl maquinasregistradaserviceImpl;
	
	@GetMapping("/maquinasregistradas")
	public List<MaquinasRegistradas> listarMaquinasRegistradas() {
		return maquinasregistradaserviceImpl.listarMaquinasRegistradas();
		
	}
	
	@PostMapping("/maquinasregistradas")
	public MaquinasRegistradas guardarMaquina(@RequestBody MaquinasRegistradas maquina) {
		return maquinasregistradaserviceImpl.guardarMaquinas(maquina);
		
	}
	
	@GetMapping("/maquinasregistradas/{id}")
	public MaquinasRegistradas maquinaID(@PathVariable(name = "id") int id) {
		
		MaquinasRegistradas maquina_id = new MaquinasRegistradas();
		
		maquina_id = maquinasregistradaserviceImpl.maquinasRegistradasID(id);
		
		System.out.println("maquina ID: " + maquina_id);
		
		return maquina_id;
		
	}
	
	@PutMapping("/maquinasregistradas/{id}")
	public MaquinasRegistradas actualizarMaquina(@PathVariable(name = "id") int id, @RequestBody MaquinasRegistradas maquina) {
		
		MaquinasRegistradas maquina_sel = new MaquinasRegistradas();
		MaquinasRegistradas maquina_act = new MaquinasRegistradas();
		
		maquina_sel = maquinasregistradaserviceImpl.maquinasRegistradasID(id);
		
		maquina_sel.setPiso(maquina.getPiso());
		
		maquina_act = maquinasregistradaserviceImpl.actualizarMaquina(maquina_sel);
		
		return maquina_act;
		
	}
	
	@DeleteMapping("/maquinasregistradas/{id}")
	public void eliminarMaquina(@PathVariable(name = "id") int id) {
		maquinasregistradaserviceImpl.eliminarMaquinas(id);
	}
}
