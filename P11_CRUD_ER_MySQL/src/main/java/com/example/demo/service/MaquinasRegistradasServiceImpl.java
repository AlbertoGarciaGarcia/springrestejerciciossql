package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IMaquinasRegistradasDAO;
import com.example.demo.dto.MaquinasRegistradas;

@Service
public class MaquinasRegistradasServiceImpl implements IMaquinasRegistradasService{

	@Autowired
	IMaquinasRegistradasDAO iMaquinasDAO;
	
	@Override
	public List<MaquinasRegistradas> listarMaquinasRegistradas() {
		// TODO Auto-generated method stub
		return iMaquinasDAO.findAll();
	}

	@Override
	public MaquinasRegistradas guardarMaquinas(MaquinasRegistradas maquinas) {
		// TODO Auto-generated method stub
		return iMaquinasDAO.save(maquinas);
	}

	@Override
	public MaquinasRegistradas maquinasRegistradasID(int id) {
		// TODO Auto-generated method stub
		return iMaquinasDAO.findById(id).get();
	}

	@Override
	public MaquinasRegistradas actualizarMaquina(MaquinasRegistradas maquinas) {
		// TODO Auto-generated method stub
		return iMaquinasDAO.save(maquinas);
	}

	@Override
	public void eliminarMaquinas(int id) {
		// TODO Auto-generated method stub
		iMaquinasDAO.deleteById(id);
	}

}
