package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Cajeros;

public interface ICajerosService {

	public List<Cajeros> listarCajeros();
	
	public Cajeros guardarCajero(Cajeros cajero);
	
	public Cajeros cajeroID(int id);
	
	public Cajeros actualizarCajero(Cajeros cajero);
	
	public void eliminarCajero(int id);
}
