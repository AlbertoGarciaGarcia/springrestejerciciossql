package com.example.demo.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "venta")
public class Venta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name = "cajero")
	Cajeros cajeros;
	
	@ManyToOne
	@JoinColumn(name = "maquina")
	MaquinasRegistradas maquinas;
	
	@ManyToOne
	@JoinColumn(name = "producto")
	Productos productos;

	//Constructores
	public Venta() {
		
	}

	/**
	 * 
	 * @param id
	 * @param cajeros
	 * @param maquinas
	 * @param productos
	 */
	public Venta(int id, Cajeros cajeros, MaquinasRegistradas maquinas, Productos productos) {
		//super();
		this.id = id;
		this.cajeros = cajeros;
		this.maquinas = maquinas;
		this.productos = productos;
	}

	
	//GettersYSetters
	/**
	 * 
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * 
	 * @return the cajeros
	 */
	public Cajeros getCajeros() {
		return cajeros;
	}

	/**
	 * 
	 * @param cajeros the cajeros to set
	 */
	public void setCajeros(Cajeros cajeros) {
		this.cajeros = cajeros;
	}

	/**
	 * 
	 * @return the maquinas
	 */
	public MaquinasRegistradas getMaquinas() {
		return maquinas;
	}

	/**
	 * 
	 * @param maquinas the maquinas to set
	 */
	public void setMaquinas(MaquinasRegistradas maquinas) {
		this.maquinas = maquinas;
	}

	/**
	 * 
	 * @return the productos
	 */
	public Productos getProductos() {
		return productos;
	}

	/**
	 * 
	 * @param productos the productos to set
	 */
	public void setProductos(Productos productos) {
		this.productos = productos;
	}

	//toString
	@Override
	public String toString() {
		return "Venta [id=" + id + ", cajeros=" + cajeros + ", maquinas=" + maquinas + ", productos=" + productos + "]";
	}
	
	
	
	
}
