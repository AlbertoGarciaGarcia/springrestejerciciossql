package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.equipos;
import com.example.demo.service.equiposImpl;

@RestController
@RequestMapping("/api")
public class equiposController {

	@Autowired
	equiposImpl equiposImpl;
	
	@GetMapping("/equipos")
	public List<equipos> listarEquipos(){
		return equiposImpl.listarEquipos();
	}
	
	@PostMapping("/equipos")
	public equipos guardarEquipos(@RequestBody equipos equipos) {
		
		return equiposImpl.guardarEquipos(equipos);
	}
	
	@GetMapping("/equipos/{id}")
	public equipos equiposXID(@PathVariable(name="id") int id) {
		
		equipos equipos_xid= new equipos();
		
		equipos_xid=equiposImpl.equiposXID(id);
		
		System.out.println("equipos XID: "+equipos_xid);
		
		return equipos_xid;
	}
	
	@PutMapping("/equipos/{id}")
	public equipos actualizarEquipos(@PathVariable(name="id")int id,@RequestBody equipos equipos) {
		
		equipos equipos_seleccionado= new equipos();
		equipos equipos_actualizado= new equipos();
		
		equipos_seleccionado= equiposImpl.equiposXID(id);
		
		equipos_seleccionado.setNombre(equipos.getNombre());
	

		
		equipos_actualizado = equiposImpl.actualizarEquipos(equipos_seleccionado);
		
		System.out.println("El equipos actualizado es: "+ equipos_actualizado);
		
		return equipos_actualizado;
	}
	
	@DeleteMapping("/equipos/{id}")
	public void eliminarEquipos(@PathVariable(name="id")int id) {
		equiposImpl.eliminarEquipos(id);
	}
}
