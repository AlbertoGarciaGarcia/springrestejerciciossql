package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.investigadores;
import com.example.demo.service.investigadoresImpl;

@RestController
@RequestMapping("/api")
public class investigadoresController {

	@Autowired
	investigadoresImpl investigadoresImpl;
	
	@GetMapping("/investigadores")
	public List<investigadores> listarInvestigadores(){
		return investigadoresImpl.listarInvestigadores();
	}
	
	@PostMapping("/investigadores")
	public investigadores guardarInvestigadores(@RequestBody investigadores investigadores) {
		
		return investigadoresImpl.guardarInvestigadores(investigadores);
	}
	
	@GetMapping("/investigadores/{id}")
	public investigadores investigadoresXID(@PathVariable(name="id") int id) {
		
		investigadores investigadores_xid= new investigadores();
		
		investigadores_xid=investigadoresImpl.investigadoresXID(id);
		
		System.out.println("investigadores XID: "+investigadores_xid);
		
		return investigadores_xid;
	}
	
	@PutMapping("/investigadores/{id}")
	public investigadores actualizarInvestigadores(@PathVariable(name="id")int id,@RequestBody investigadores investigadores) {
		
		investigadores investigadores_seleccionado= new investigadores();
		investigadores investigadores_actualizado= new investigadores();
		
		investigadores_seleccionado= investigadoresImpl.investigadoresXID(id);
		
		investigadores_seleccionado.setNomApels(investigadores.getNomApels());
		investigadores_seleccionado.setFacultad(investigadores.getFacultad());

		
		investigadores_actualizado = investigadoresImpl.actualizarInvestigadores(investigadores_seleccionado);
		
		System.out.println("El investigadores actualizado es: "+ investigadores_actualizado);
		
		return investigadores_actualizado;
	}
	
	@DeleteMapping("/investigadores/{id}")
	public void eliminarInvestigadores(@PathVariable(name="id")int id) {
		investigadoresImpl.eliminarInvestigadores(id);
	}
}
