package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.reserva;
import com.example.demo.service.reservaImpl;

@RestController
@RequestMapping("/api")
public class reservaController {

	@Autowired
	reservaImpl reservaImpl;
	
	@GetMapping("/reserva")
	public List<reserva> listarReserva(){
		return reservaImpl.listarReserva();
	}
	
	@PostMapping("/reserva")
	public reserva guardarReserva(@RequestBody reserva reserva) {
		
		return reservaImpl.guardarReserva(reserva);
	}
	
	@GetMapping("/reserva/{id}")
	public reserva reservaXID(@PathVariable(name="id") int id) {
		
		reserva reserva_xid= new reserva();
		
		reserva_xid=reservaImpl.reservaXID(id);
		
		System.out.println("reserva XID: "+reserva_xid);
		
		return reserva_xid;
	}
	
	@PutMapping("/reserva/{id}")
	public reserva actualizarReserva(@PathVariable(name="id")int id,@RequestBody reserva reserva) {
		
		reserva reserva_seleccionado= new reserva();
		reserva reserva_actualizado= new reserva();
		
		reserva_seleccionado= reservaImpl.reservaXID(id);
		
		reserva_seleccionado.setComienzo(reserva.getComienzo());
		reserva_seleccionado.setFin(reserva.getFin());
		reserva_seleccionado.setInvestigadores(reserva.getInvestigadores());
		reserva_seleccionado.setEquipos(reserva.getEquipos());
	

		
		reserva_actualizado = reservaImpl.actualizarReserva(reserva_seleccionado);
		
		System.out.println("El reserva actualizado es: "+ reserva_actualizado);
		
		return reserva_actualizado;
	}
	
	@DeleteMapping("/reserva/{id}")
	public void eliminarReserva(@PathVariable(name="id")int id) {
		reservaImpl.eliminarReserva(id);
	}
}
