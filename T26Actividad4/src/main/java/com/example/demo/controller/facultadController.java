package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.facultad;
import com.example.demo.service.facultadImpl;

@RestController
@RequestMapping("/api")
public class facultadController {

	@Autowired
	facultadImpl facultadImpl;
	
	@GetMapping("/facultad")
	public List<facultad> listarFacultad(){
		return facultadImpl.listarfacultad();
	}
	
	@PostMapping("/facultad")
	public facultad guardarFacultad(@RequestBody facultad facultad) {
		
		return facultadImpl.guardarfacultad(facultad);
	}
	
	@GetMapping("/facultad/{id}")
	public facultad facultadXID(@PathVariable(name="id") int id) {
		
		facultad facultad_xid= new facultad();
		
		facultad_xid=facultadImpl.facultadXID(id);
		
		System.out.println("facultad XID: "+facultad_xid);
		
		return facultad_xid;
	}
	
	@PutMapping("/facultad/{id}")
	public facultad actualizarFacultad(@PathVariable(name="id")int id,@RequestBody facultad facultad) {
		
		facultad facultad_seleccionado= new facultad();
		facultad facultad_actualizado= new facultad();
		
		facultad_seleccionado= facultadImpl.facultadXID(id);
		
		facultad_seleccionado.setNombre(facultad.getNombre());
	

		
		facultad_actualizado = facultadImpl.actualizarfacultad(facultad_seleccionado);
		
		System.out.println("El facultad actualizado es: "+ facultad_actualizado);
		
		return facultad_actualizado;
	}
	
	@DeleteMapping("/facultad/{id}")
	public void eliminarFacultad(@PathVariable(name="id")int id) {
		facultadImpl.eliminarfacultad(id);
	}
}
