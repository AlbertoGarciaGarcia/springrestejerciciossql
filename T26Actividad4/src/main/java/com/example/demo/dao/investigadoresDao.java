package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.dto.investigadores;

public interface investigadoresDao extends JpaRepository<investigadores, Integer> {

}
