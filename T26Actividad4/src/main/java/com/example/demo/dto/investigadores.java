package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="investigadores")
public class investigadores {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "nomapels")
	private String nomApels;
	
	@ManyToOne
	@JoinColumn(name="facultad")
	private facultad facultad;
	
	@OneToMany
	@JoinColumn(name="id")
	private List<reserva> reserva;
	
	public investigadores() {
		
	}
	
	public investigadores(int id, String nomApels) {
		this.id = id;
		this.nomApels = nomApels;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomApels() {
		return nomApels;
	}

	public void setNomApels(String nomApels) {
		this.nomApels = nomApels;
	}

	public facultad getFacultad() {
		return facultad;
	}

	public void setFacultad(facultad facultad) {
		this.facultad = facultad;
	}

	@Override
	public String toString() {
		return "investigadores [id=" + id + ", nombre=" + nomApels + ", facultad=" + facultad + "]";
	}
	
	
}

