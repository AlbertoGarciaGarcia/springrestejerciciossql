package com.example.demo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="reserva")
public class reserva {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "comienzo")
	private String comienzo;
	@Column(name = "fin")
	private String fin;
	
	@ManyToOne
	@JoinColumn(name="idinvestigadores")
	private investigadores idinvestigadores;
	
	@ManyToOne
	@JoinColumn(name="idequipos")
	private equipos idequipos;

	
	public reserva() {
		
	}
	
	public reserva(int id, String comienzo, String fin) {
		this.id = id;
		this.comienzo = comienzo;
		this.fin = fin;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getComienzo() {
		return comienzo;
	}

	public void setComienzo(String comienzo) {
		this.comienzo = comienzo;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	public investigadores getInvestigadores() {
		return idinvestigadores;
	}

	public void setInvestigadores(investigadores investigadores) {
		this.idinvestigadores = investigadores;
	}

	public equipos getEquipos() {
		return idequipos;
	}

	public void setEquipos(equipos equipos) {
		this.idequipos = equipos;
	}

	@Override
	public String toString() {
		return "reserva [id=" + id + ", comienzo=" + comienzo + ", fin=" + fin + ", investigadores=" + idinvestigadores
				+ ", equipos=" + idequipos + "]";
	}
	
}
