package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;




@Entity
@Table(name="facultad")
public class facultad {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "nombre")
	private String nombre;
	
	
	@OneToMany
	@JoinColumn(name="id")
	private List<investigadores> investigadores;
	
	@OneToMany
	@JoinColumn(name="id")
	private List<equipos> equipos;
	
	
	
	public facultad() {
		
	}
	
	public facultad(int id, String nombre) {
		this.id= id;
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "facultad [id=" + id + ", nombre=" + nombre + "]";
	}
	
	
}
