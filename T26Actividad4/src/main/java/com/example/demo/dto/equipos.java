package com.example.demo.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="equipos")
public class equipos {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name = "nombre")
	private String nombre;
	
	@ManyToOne
	@JoinColumn(name="facultad")
	private facultad facultad;
	
	@OneToMany
	@JoinColumn(name="id")
	private List<reserva> reserva;

	public equipos() {
		
	}
	
	public equipos(int id, String nombre) {
		this.id= id;
		this.nombre = nombre;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public facultad getFacultad() {
		return facultad;
	}

	public void setFacultad(facultad facultad) {
		this.facultad = facultad;
	}

	@Override
	public String toString() {
		return "equipos [id=" + id + ", nombre=" + nombre + ", facultad=" + facultad + "]";
	}
	
	
}
