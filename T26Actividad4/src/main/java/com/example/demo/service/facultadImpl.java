package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.facultadDao;
import com.example.demo.dto.facultad;
@Service
public class facultadImpl implements facultadService{
	
	@Autowired
	facultadDao facultadDao;
	
	@Override
	public List<facultad> listarfacultad() {
		
		return facultadDao.findAll();
	}

	@Override
	public facultad guardarfacultad(facultad facultad) {
		
		return facultadDao.save(facultad);
	}

	@Override
	public facultad facultadXID(int id) {
		
		return facultadDao.findById(id).get();
	}

	@Override
	public facultad actualizarfacultad(facultad facultad) {
		
		return facultadDao.save(facultad);
	}

	@Override
	public void eliminarfacultad(int id) {
		
		facultadDao.deleteById(id);
		
	}	
}
