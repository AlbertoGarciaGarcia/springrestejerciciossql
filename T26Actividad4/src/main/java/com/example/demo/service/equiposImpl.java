package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.equiposDao;
import com.example.demo.dto.equipos;
@Service
public class equiposImpl implements equiposService{
	
	@Autowired
	equiposDao equiposDao;
	
	@Override
	public List<equipos> listarEquipos() {
		return equiposDao.findAll();
	}

	@Override
	public equipos guardarEquipos(equipos equipos) {
		return equiposDao.save(equipos);
	}

	@Override
	public equipos equiposXID(int id) {
		return equiposDao.findById(id).get();
	}

	@Override
	public equipos actualizarEquipos(equipos equipos) {
		return equiposDao.save(equipos);
	}

	@Override
	public void eliminarEquipos(int id) {
		equiposDao.deleteById(id);
		
	}	
}
