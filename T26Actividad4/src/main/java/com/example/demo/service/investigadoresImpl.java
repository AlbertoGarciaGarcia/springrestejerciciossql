package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.investigadoresDao;
import com.example.demo.dto.investigadores;
@Service
public class investigadoresImpl implements investigadoresService{
	
	@Autowired
	investigadoresDao investigadoresDao;
	
	@Override
	public List<investigadores> listarInvestigadores() {
		
		return investigadoresDao.findAll();
	}

	@Override
	public investigadores guardarInvestigadores(investigadores investigadores) {
		
		return investigadoresDao.save(investigadores);
	}

	@Override
	public investigadores investigadoresXID(int id) {
		
		return investigadoresDao.findById(id).get();
	}

	@Override
	public investigadores actualizarInvestigadores(investigadores investigadores) {
		
		return investigadoresDao.save(investigadores);
	}

	@Override
	public void eliminarInvestigadores(int id) {
		
		investigadoresDao.deleteById(id);
		
	}	
}
