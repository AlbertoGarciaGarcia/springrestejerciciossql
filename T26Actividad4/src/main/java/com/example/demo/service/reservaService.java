package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.reserva;

public interface reservaService {
public List<reserva> listarReserva(); //Listar All 
	
	public reserva guardarReserva(reserva reserva);	//Guarda un reserva CREATE
	
	public reserva reservaXID(int id); //Leer datos de un reserva READ
	
	public reserva actualizarReserva(reserva reserva); //Actualiza datos del reserva UPDATE
	
	public void eliminarReserva(int id);// Elimina el reserva DELETE
}
