package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.facultad;


public interface facultadService {
public List<facultad> listarfacultad(); //Listar All 
	
	public facultad guardarfacultad(facultad facultad);	//Guarda un facultad CREATE
	
	public facultad facultadXID(int id); //Leer datos de un facultad READ
	
	public facultad actualizarfacultad(facultad facultad); //Actualiza datos del facultad UPDATE
	
	public void eliminarfacultad(int id);// Elimina el facultad DELETE
}
