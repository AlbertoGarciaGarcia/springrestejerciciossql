package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.investigadores;

public interface investigadoresService {
public List<investigadores> listarInvestigadores(); //Listar All 
	
	public investigadores guardarInvestigadores(investigadores investigadores);	//Guarda un investigadores CREATE
	
	public investigadores investigadoresXID(int id); //Leer datos de un investigadores READ
	
	public investigadores actualizarInvestigadores(investigadores investigadores); //Actualiza datos del investigadores UPDATE
	
	public void eliminarInvestigadores(int id);// Elimina el investigadores DELETE
}

