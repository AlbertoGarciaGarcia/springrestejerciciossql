package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.reservaDao;
import com.example.demo.dto.reserva;
@Service
public class reservaImpl implements reservaService{
	
	@Autowired
	reservaDao reservaDao;
	
	@Override
	public List<reserva> listarReserva() {
		
		return reservaDao.findAll();
	}

	@Override
	public reserva guardarReserva(reserva reserva) {
		
		return reservaDao.save(reserva);
	}

	@Override
	public reserva reservaXID(int id) {
		
		return reservaDao.findById(id).get();
	}

	@Override
	public reserva actualizarReserva(reserva reserva) {
		
		return reservaDao.save(reserva);
	}

	@Override
	public void eliminarReserva(int id) {
		
		reservaDao.deleteById(id);
		
	}	
}
