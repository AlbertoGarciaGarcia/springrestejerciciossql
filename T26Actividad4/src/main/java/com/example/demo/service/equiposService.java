package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.equipos;


public interface equiposService {
public List<equipos> listarEquipos(); //Listar All 
	
	public equipos guardarEquipos(equipos equipos);	//Guarda un equipos CREATE
	
	public equipos equiposXID(int id); //Leer datos de un equipos READ
	
	public equipos actualizarEquipos(equipos equipos); //Actualiza datos del equipos UPDATE
	
	public void eliminarEquipos(int id);// Elimina el equipos DELETE
}
